/**
 * Created by golushko.p on 29.09.2017.
 */
const cryptosToShow = {
    'currency': ['USD', 'RUB', 'EUR'],
    'cryptos': ['BTC', 'ETH', 'LTC', 'DSH']
};
export default cryptosToShow;