let apiUrls = {
    totalDeposites: 'http://service.ogeorgy.me/api/total_deposits',
    registration: 'http://service.ogeorgy.me/api/sign_up',
    authorization: 'http://service.ogeorgy.me//api/sign_in',
};
export default apiUrls;