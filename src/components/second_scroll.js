import React from 'react';
import ActionButton from "./call_to_action_button";
import {AreaChart, Area, XAxis, YAxis, Tooltip, PieChart, Pie, Label, Legend, LabelList, Cell, ReferenceDot} from 'recharts';
import getDataService from '../services/get-data-service'

class SecondScroll extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        this.getTotalData()
    }

    async getTotalData() {
        let data = await getDataService.getTotalData();
        console.log(data)
    }


    data = [
        {'Amount': 1, 'date': '21.01'},
        {'Amount': 2, 'date': '22.01'},
        {'Amount': 5, 'date': '23.01'},
        {'Amount': 6, 'date': '24.01'}
    ];
   data_pie = [
      { name: 'BTC', value: 22.045 },
      { name: 'ETH', value: 10.342 },
      { name: 'LTC', value: 4.005 },
    ];

   colors = ['#c41c00', '#ff8a50', '#ff5722' ];


  renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index, name }) => {
      const RADIAN = Math.PI / 180;
      const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
      let x  = cx + radius * Math.cos(-midAngle * RADIAN);
      let y = cy  + radius * Math.sin(-midAngle * RADIAN);
      return (
        <text style={{'fontSize': '20px'}} x={x} y={y} fill="white" textAnchor='middle' dominantBaseline="central">
            {name}
        </text>
      );
    };

  renerAreaLabel = (props) => {
    if (props.index !== 0) {
        return (
            <g>
                {/*<circle cx={props.x} cy={props.y - 20} stroke='#00bcd4' fill='transparent' r={15}/>*/}
                <text x={props.x} y={props.y - 15} stroke='#00bcd4' fontSize='14px'
                      textAnchor="middle">&#8776; {props.value}$</text>
            </g>

        )
    }
  };

    render () {
        return (
            <div id='statistics' className='white_accent_back'>
                <div className='container'>
                    <div className='second_scroll'>
                        <div className='max-width text-center primary_color block_header'>STATISTICS</div>
                        <div className='charts'>
                            <div className='deposites_chart'>
                                <AreaChart  width={600} height={400} data={this.data} margin={{right: 20}}>
                                    <Area type='monotone' dataKey='Amount' stroke='#c41c00'
                                          fill='#ff5722' strokeWidth={2} label={this.renerAreaLabel} />
                                    <XAxis dataKey="date"/>
                                    <YAxis/>
                                    <Tooltip/>
                                </AreaChart>
                            </div>
                            <div className='pie_chart flex-outer flex-column justify-center'>
                                <PieChart width={300} height={300}>
                                    {/*<Legend verticalAlign='bottom'>1000+ INVESTORS</Legend>*/}
                                    <Pie
                                      data={this.data_pie}
                                      dataKey="value"
                                      cx={150}
                                      cy={150}
                                      startAngle={0}
                                      endAngle={360}
                                      outerRadius={120}
                                      innerRadius={40}
                                      labelLine={false}
                                      label={this.renderCustomizedLabel}
                                    >
                                        {this.data_pie.map((x, index) => {
                                            return (
                                                <Cell key={'cell-' + index} fill={this.colors[index]} stroke='transparent'/>
                                            )
                                        })}

                                        {/*<LabelList dataKey='name' position='inside' content={this.renderLabel}/>*/}
                                    </Pie>
                                    <Tooltip/>
                                </PieChart>
                                <div className='piechart_text text-center'><span className='bold'>1000+</span> DEPOSITES</div>
                            </div>
                        </div>
                    </div>
                </div>
                <ActionButton name='BECOME INVESTOR'/>
            </div>
        )
    }
}
export default SecondScroll