import React from 'react';
// import store from '../store';
import {FlatButton} from 'material-ui'
import Dialog from 'material-ui/Dialog'
import RegistrationForm from "./registration_form";
import getDataService from '../services/get-data-service'
import AuthorizationForm from "./authorization_form";
import store from "../store";
import actionTypes from '../models/action-types';


const Logo = (props)=><div className={props.className}>
    <span className={props.innerClass}>{props.name}</span></div>;
const Button =(props)=> <div className='before_button'><FlatButton onClick={props.callbackClickFunction}> {props.name}</FlatButton></div>;

class Header extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            sign_in: false,
            sign_up: false,
            user: {}
        };
        store.subscribe((event) => {console.log(store.getState())})
    }

    componentDidMount() {
        // store.subscribe(() => this.forceUpdate());

    }

    clickAnchorFunction(destination, event) {
        const   scrollHeight = document.getElementById(destination).offsetTop,
        scrollStep = Math.PI / ( 900 / 15 ),
        cosParameter = scrollHeight / 2;
        let scrollCount = 0;
        let scrollInterval = setInterval( function() {
            if ( window.scrollY != document.getElementById(destination).offsetTop) {
                scrollCount = scrollCount + 1;
                let scrollMargin = cosParameter - cosParameter * Math.cos( scrollCount * scrollStep );
                window.scrollTo( 0, ( scrollMargin ) );
            }
            else clearInterval(scrollInterval);
        }, 15 );
    }

    clickSignInFunction() {
        this.setState({sign_in: true});
    }
    clickSignUpFunction() {
        this.setState({sign_up: true});
    }

    handleClose() {
        console.log(this);
        this.setState({sign_in: false, sign_up: false});
        console.log(this);
    }

    async handleTryRegistration(state) {
        let data = {
            username: state.nickname,
            password: state.password,
            email: state.email,
            referral: state.referal || ''
        };
        let registr = await getDataService.registration(data);
        if (registr.status === 201) {
            this.userIsloggedIn(registr.data);
            this.handleClose()
        }
    }
    async handleTryAuthorization(state) {
        let data = {
            username: state.nickname,
            password: state.password,
        };
        let registr = await getDataService.authorization(data);

        console.log(registr);
        if (registr.status === 200) {
            this.userIsloggedIn(registr.data);
            this.handleClose()
        }
    }

    userIsloggedIn(data) {
        store.dispatch({type: actionTypes.successfullLogin, user_data: data});
        console.log(store.getState());
    }

    render() {
        const logged_in = store.getState()['cryptoReducer']['loggedIn'];
        const user = store.getState()['cryptoReducer']['user'];

        const authorization_area = (logged_in)=>{
            if (!logged_in) {
                return (
                    <div className='flex-outer justify-between'>
                        <Button callbackClickFunction={this.clickSignInFunction.bind(this)} name='SIGN IN'/>
                        <Button callbackClickFunction={this.clickSignUpFunction.bind(this)} name='SIGN UP'/>
                    </div>
                )
            } else {
                return (
                    <div className='flex-outer flex-column registration_area'>
                        <div className='flex-outer'><i className='material-icons'>person</i></div>
                        <div className='flex-outer max-width'><span className='margin-auto primary_color'>{user}</span></div>
                    </div>
                )
            }
        }

        return (
            <div className='project_header'>
                <div className="container flex-outer max-height">
                    <div className='flex-outer justify-between max-width margin-auto'>
                        <div className='flex-outer'>
                            <div className='margin-auto logo'>
                                INVEST CO
                            </div>
                        </div>
                        <div className='flex-outer'>
                            <div className='margin-auto-0 flex-outer justify-around'>
                                <Button name='STATISTICS' callbackClickFunction={this.clickAnchorFunction.bind(this, 'statistics')}/>
                                <Button name='OFFER' callbackClickFunction={this.clickAnchorFunction.bind(this, 'offer')}/>
                                <Button name='CONTACT US' callbackClickFunction={this.clickAnchorFunction.bind(this, 'contact_us')}/>
                            </div>
                        </div>
                        {authorization_area(logged_in)}
                    </div>
                </div>
                <Dialog
                    open={this.state.sign_in}
                    onRequestClose={this.handleClose.bind(this)}
                    contentStyle={{'width': '40%'}}
                >
                    <RegistrationForm tryRegistration={this.handleTryRegistration.bind(this)} cancelButton={this.handleClose.bind(this)}/>
                </Dialog>
                <Dialog
                    open={this.state.sign_up}
                    onRequestClose={this.handleClose.bind(this)}
                    contentStyle={{'width': '40%'}}
                >
                    <AuthorizationForm tryRegistration={this.handleTryAuthorization.bind(this)} cancelButton={this.handleClose.bind(this)}/>
                </Dialog>
            </div>
        )
    }
}
export default Header;