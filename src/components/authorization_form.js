import React from 'react';
import TextField from 'material-ui/TextField'
import {FlatButton} from 'material-ui'

class AuthorizationForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            nickname: '',
            password: '',
            password_input_type: 'password',
            general_err: 'THIS FIELD IS REQUIRED',
            nickname_err: '',
            password_err: '',
        }
    }

    onInputChange = (event)=> {
        if (event.target.id === 'nickname_input_authorization_form') {
            this.setState({
                nickname: event.target.value,
                nickname_err: event.target.value ? '' : this.state.general_err
            });
        } else if (event.target.id === 'password_input_authorization_form') {
            this.setState({
                password: event.target.value,
                password_err: event.target.value ? '' : this.state.general_err
            });
        }
    };

    cancelButton() {
        this.props.cancelButton();
    }

    RegistrationButton() {
        if (this.checkIfAllRequiredFieldsEntered()) {
            this.props.tryRegistration(this.state)
        }
    }

    checkIfAllRequiredFieldsEntered() {
        this.setState({
                nickname_err: this.state.nickname ? '' : this.state.general_err,
                password_err: this.state.password ? '' : this.state.general_err
            });
        if (!this.state.nickname || !this.state.password) {
            return false
        } else {
            return true
        }
    }

    render () {
        const actions = [
                <FlatButton key='cancel_button_in_registration_form' label='Cancel' onClick={this.cancelButton.bind(this)}/>,
                <FlatButton key='confirm_button_in_registration_form' label='Submit' onClick={this.RegistrationButton.bind(this)}/>
        ];

        return (
            <div className='registration' id='registration_form'>
                <div className='flex-outer justify-center'>AUTHORIZATION</div>
                <div className='flex-outer flex-column'>
                    <div className='flex-outer'>
                        <div className='flex-outer flex-column justify-end icon_block'>
                            <i className='material-icons'>perm_identity</i>
                        </div>
                        <TextField floatingLabelText='NICKNAME*:' id='nickname_input_authorization_form' value={this.state.nickname}
                                       fullWidth={true} onChange={this.onInputChange}
                                       underlineFocusStyle={{'borderBottom':'2px solid rgb(0, 188, 212)'}}
                                       underlineStyle={{'borderBottom':'1px solid rgb(0, 188, 212)'}}
                                       inputStyle={{'color': '#008ba3'}}
                        />
                    </div>
                    <div className='flex-outer'>
                        <div className='flex-outer flex-column justify-end icon_block'>
                            <i className='material-icons'>security</i>
                        </div>
                    <TextField floatingLabelText='PASSWORD*:' id='password_input_authorization_form' value={this.state.password}
                                       fullWidth={true} onChange={this.onInputChange}
                                       underlineFocusStyle={{'borderBottom':'2px solid rgb(0, 188, 212)'}}
                                       underlineStyle={{'borderBottom':'1px solid rgb(0, 188, 212)'}}
                                       inputStyle={{'color': '#008ba3'}} type={this.state.password_input_type}/>
                    </div>

                    <div className='flex-outer justify-between actions_block'>{actions}</div>
                </div>
            </div>
        )
    }
}
export default AuthorizationForm