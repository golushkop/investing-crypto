import React from 'react';
import Header from "./header";
import ActionButton from "./call_to_action_button";

class FirstScroll extends React.Component {
    render () {
        return (
            <div>
                <div className='primary_back first_page'>
                    <Header/>
                    <div className="container">
                        <div className='first_scroll_body'>
                            <div className='first_scroll_body_header'>Make money while you can</div>
                            <div className='first_scroll_body_text'>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna
                                aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
                                aute irure dolor in reprehenderit in voluptate velit esse cillum
                                dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                                non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</div>
                            <ActionButton name='INVESTING'/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default FirstScroll