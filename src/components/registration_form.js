import React from 'react';
import TextField from 'material-ui/TextField'
import {FlatButton} from 'material-ui'

class RegistrationForm extends React.Component {

    constructor(props) {
        super(props);
        this.state =  {
            nickname: '',
            password: '',
            email: '',
            referal: '',
            password_input_type: 'password',
            general_err: 'THIS FIELD IS REQUIRED',
            nickname_err: '',
            password_err: '',
            email_err: '',
        }
    }

    componentDidMount() {
        console.log(this.state)
    }

    onInputChange = (event)=> {
        if (event.target.id === 'nickname_input_registration_form') {
            this.setState({
                nickname: event.target.value,
                nickname_err: event.target.value ? '' : this.state.general_err
            });
        } else if (event.target.id === 'password_input_registration_form') {
            this.setState({
                password: event.target.value,
                password_err: event.target.value ? '' : this.state.general_err
            });
        } else if (event.target.id === 'email_input_registration_form') {
            this.setState({
                email: event.target.value,
                email_err: event.target.value ? '' : this.state.general_err
            });
        }
        else if (event.target.id === 'referal_input_registration_form') {
            this.setState({
                referal: event.target.value,
            });
        }
    };

    registrationButton() {
        if (this.checkIfAllRequiredFieldsEntered()) {
            this.props.tryRegistration(this.state)
        }
    }

    checkIfAllRequiredFieldsEntered() {
        this.setState({
                nickname_err: this.state.nickname ? '' : this.state.general_err,
                password_err: this.state.password ? '' : this.state.general_err,
                email_err: this.state.email ? '' : this.state.general_err,
            });
        if (!this.state.nickname || !this.state.email || !this.state.password) {
            return false
        } else {
            return true
        }
    }

    cancelButton() {
        this.props.cancelButton()
    }

    render () {
        const actions = [
                <FlatButton key='cancel_button_in_registration_form' label='Cancel' onClick={this.cancelButton.bind(this)}/>,
                <FlatButton key='confirm_button_in_registration_form' label='Submit' onClick={this.registrationButton.bind(this)}/>
        ];

        return (
            <div className='registration' id='registration_form'>
                <div className='flex-outer justify-center'>REGISTRATION</div>
                <div className='flex-outer flex-column'>
                    <div className='flex-outer'>
                        <div className='flex-outer flex-column justify-end icon_block'>
                            <i className='material-icons'>perm_identity</i>
                        </div>
                        <TextField floatingLabelText='NICKNAME*:' id='nickname_input_registration_form' value={this.state.nickname}
                                   fullWidth={true} onChange={this.onInputChange}
                                   underlineFocusStyle={{'borderBottom':'2px solid rgb(0, 188, 212)'}}
                                   underlineStyle={{'borderBottom':'1px solid rgb(0, 188, 212)'}}
                                   inputStyle={{'color': '#008ba3'}} errorText={this.state.nickname_err}
                        />
                    </div>
                    <div className='flex-outer'>
                        <div className='flex-outer flex-column justify-end icon_block'>
                            <i className='material-icons'>security</i>
                        </div>
                    <TextField floatingLabelText='PASSWORD*:' id='password_input_registration_form' value={this.state.password}
                               fullWidth={true} onChange={this.onInputChange}
                               underlineFocusStyle={{'borderBottom':'2px solid rgb(0, 188, 212)'}}
                               underlineStyle={{'borderBottom':'1px solid rgb(0, 188, 212)'}}
                               inputStyle={{'color': '#008ba3'}}
                               type={this.state.password_input_type}
                               errorText={this.state.password_err}
                    />
                        {/*<FlatButton className='primary_color' label='show'/>*/}
                    </div>
                    <div className='flex-outer'>
                        <div className='flex-outer flex-column justify-end icon_block'>
                            <i className='material-icons'>mail_outline</i>
                        </div>
                    <TextField floatingLabelText='EMAIL*:' id='email_input_registration_form' value={this.state.email}
                               fullWidth={true} onChange={this.onInputChange}
                               underlineFocusStyle={{'borderBottom':'2px solid rgb(0, 188, 212)'}}
                               underlineStyle={{'borderBottom':'1px solid rgb(0, 188, 212)'}}
                               inputStyle={{'color': '#008ba3'}}
                               errorText={this.state.email_err}
                    />
                    </div>
                    <div className='flex-outer'>
                        <div className='flex-outer flex-column justify-end icon_block'>
                            <i className='material-icons'>group</i>
                        </div>
                    <TextField floatingLabelText='REFERAL (if applyed):' id='referal_input_registration_form' value={this.state.referal}
                                       fullWidth={true} onChange={this.onInputChange}
                                       underlineFocusStyle={{'borderBottom':'2px solid rgb(0, 188, 212)'}}
                                       underlineStyle={{'borderBottom':'1px solid rgb(0, 188, 212)'}}
                                       inputStyle={{'color': '#008ba3'}}/>
                    </div>

                    <div className='flex-outer justify-between actions_block'>{actions}</div>
                </div>
            </div>
        )
    }
}
export default RegistrationForm