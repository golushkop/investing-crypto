import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FirstScroll from "./first_scoll";
import SecondScroll from "./second_scroll";
import ThirdScroll from "./third_scroll";
import FourthScroll from "./fourth_scroll";

class MainPage extends React.Component {

    componentDidMount() {
        console.log(this.props)
    }


    render() {
        return (
            <div>
                <FirstScroll/>
                <SecondScroll/>
                <ThirdScroll/>
                <FourthScroll/>
            </div>
        )
    }
}
export default MainPage;