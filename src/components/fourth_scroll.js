import React from 'react'
import TextField from 'material-ui/TextField'
import ActionButton from "./call_to_action_button";

class FourthScroll extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            mail: '',
            message: ''
        }
    }


    onInputChange = (event)=> {
        if (event.target.id === 'name_input_contact_form') {
            this.setState({
                name: event.target.value,
            });
        } else if (event.target.id === 'mail_input_contact_form') {
            this.setState({
                mail: event.target.value,
            });
        } else if (event.target.id === 'message_input_contact_form') {
            this.setState({
                message: event.target.value,
            });
        }
    };

    // checkTest() {
    //     console.log(this.state)
    // }

    render () {
        return (
            <div id='contact_us' className='white_accent_back'>
                <div className='container'>
                    <div className='fourth_scroll'>
                        <div className='max-width text-center primary_color block_header'>CONTACT US</div>
                        <div className='contact_us_form flex-outer flex-column margin-auto'>
                            <TextField floatingLabelText='NAME:' id='name_input_contact_form' value={this.state.name}
                                       onChange={this.onInputChange} fullWidth={true}
                                       underlineFocusStyle={{'borderBottom':'2px solid rgb(0, 188, 212)'}}
                                       underlineStyle={{'borderBottom':'1px solid rgb(0, 188, 212)'}}
                                       inputStyle={{'color': '#008ba3'}}
                            />
                            <TextField floatingLabelText='EMAIL:' id='mail_input_contact_form' value={this.state.mail}
                                       onChange={this.onInputChange} fullWidth={true}
                                       underlineFocusStyle={{'borderBottom':'2px solid rgb(0, 188, 212)'}}
                                       underlineStyle={{'borderBottom':'1px solid rgb(0, 188, 212)'}}
                                       inputStyle={{'color': '#008ba3'}}
                            />
                            <TextField floatingLabelText='MESSAGE:' id='message_input_contact_form'
                                       value={this.state.message}
                                       onChange={this.onInputChange}
                                       fullWidth={true}
                                       multiLine={true}
                                       rows={6}
                                       rowsMax={10}
                                       underlineFocusStyle={{'border':'2px solid rgb(0, 188, 212)', 'height': '162px',
                                           'transition': 'all 0.4s ease-in-out', 'bottom': '11px', 'width': '596px'}}
                                       // underlineStyle={{'border':'1px solid rgb(0, 188, 212)', 'height': '144px'}}
                                       underlineStyle={{'bottom': '11px'}}
                                       textareaStyle={{'border':'1px solid rgb(0, 188, 212)', 'padding': '10px', 'color': '#008ba3'}}
                                       floatingLabelFixed={true}
                            />

                            {/*<button onClick={this.checkTest.bind(this)}>Check</button>*/}
                        </div>
                    </div>
                </div>
                <ActionButton name='SEND MESSAGE'/>
            </div>
        )
    }
}
export default FourthScroll