import React, { Component } from 'react';
import Main from './Main';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

class App extends Component {
  render() {
    return (
      <div className="App">
        <MuiThemeProvider>
          <Main message="Main app"/>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default App;
