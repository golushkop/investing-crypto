import React from 'react'
import Header from './header';
import {Route} from 'react-router-dom';
import MainPage from './main_page';
class Main extends React.Component {
    render() {
        return (
            <div className="">
                <div className="first-page">
                    <Route exact path='/' component={MainPage}/>
                </div>
            </div>
        )
    }
}
export default Main;