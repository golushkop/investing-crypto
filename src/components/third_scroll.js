import React from 'react';
import ActionButton from "./call_to_action_button";


const Block = (props)=> {
    return (
        <div className='flex-outer flex-column offer_block'>
            <div className='offer_block_header'>{props.percents}% DAILY</div>
            <div className='offer_block_body flex-column flex-outer'>
                <div className='offer_block_subheader'>DEPOSIT</div>
                <div className='offer_block_value flex-outer justify-around'>
                    <i className='cc BTC'/> <span className='margin-auto'>{props.BTC}</span>
                </div>
                    <div className='offer_block_value flex-outer justify-around'>
                        <i className='cc ETH'/> <span className='margin-auto'>{props.ETH}</span>
                    </div>
                    <div className='offer_block_value flex-outer justify-around'>
                        <i className='cc LTC'/> <span className='margin-auto'>{props.LTC}</span>
                    </div>
                <div className='offer_block_subheader'>MONTHLY ROI</div>
                <div className='offer_block_value text-center bold'>{props.ROI} %</div>
                <div className='offer_block_subheader'>PAYMENTS</div>
                <div className='offer_block_value text-center bold'>{props.PAYMENTS}</div>
            </div>
        </div>
    )

};

class ThirdScroll extends React.Component {

    render () {
        return (
            <div id='offer' className='primary_back third_scroll'>
                <div className='container'>
                    <div className='max-width text-center block_header color_white'>OFFER</div>
                    <div className='flex-outer justify-between blocks_container'>
                        <Block percents='2' BTC='0.001 - 0.010' ETH='0.010 - 0.050' LTC='0.050 - 0.080' ROI='177' PAYMENTS='DAILY'/>
                        <Block percents='2.5' BTC='0.010 - 0.100' ETH='0.050 - 0.200' LTC='0.080 - 0.250' ROI='205' PAYMENTS='DAILY'/>
                        <Block percents='3' BTC='>0.100' ETH='>0.200' LTC='>0.250' ROI='235' PAYMENTS='DAILY'/>
                    </div>
                </div>
                <ActionButton name='SIGN IN'/>
            </div>
        )
    }
}

export default ThirdScroll