import React from 'react'
import RaisedButton from 'material-ui/RaisedButton';
class ActionButton extends React.Component {
    constructor(props) {
        super(props);
    }
    render () {
        return (
            <div className='flex-outer max-width action_button_row'>
                <div className='margin-auto action_button'>
                    <button>{this.props.name || 'INVESTING'}</button>
                </div>
            </div>
        )
    }
}
export default ActionButton