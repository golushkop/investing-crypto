/**
 * Created by golushko.p on 29.09.2017.
 */
import actionTypes from '../models/action-types';

const initialState = {
    loggedIn: false,
    authentication: '',
    user: ''
};
const cryptoReducer  = function (state = initialState, action) {
    let newState = state;

    switch (action.type) {

        case actionTypes.successfullLogin:
            console.log(action);
            newState.loggedIn = true;
            newState.authentication = action.user_data.authentication;
            newState.user = action.user_data.username;
            return newState;

        default:
            return newState;

    }
};
export default cryptoReducer;