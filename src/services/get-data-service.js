/**
 * Created by golushko.p on 29.09.2017.
 */
import axios from 'axios';
import apiUrls from '../models/apiUrls';

export default class getData {

    static async getTotalData() {
        let config =
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            };
        const response = await axios.get(apiUrls.totalDeposites, config);
        return response;
    }

    static async registration(_data) {
        const response = await axios.post(apiUrls.registration, JSON.stringify(_data), {headers: { 'Content-Type': 'application/json'}});
        return response;
    }
    static async authorization(_data) {
        const response = await axios.post(apiUrls.authorization, '', {
            headers: { 'Content-Type': 'application/json'},
            auth: {username: _data.username, password: _data.password}
        });
        return response;
    }
}